/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "config" }] */

// config object with list of available words for this exercise
// in the integrated version this will be generated dynamically by the backend
const config = {
  words: [
    { full: 'gearbeitet', syllables: ['ge', 'ar', 'bei', 'tet'] },
    { full: 'Erntehelfer', syllables: ['Ern', 'te', 'hel', 'fer'] },
    { full: 'Baustelle', syllables: ['Bau', 'stel', 'le'] },
    { full: 'Haushaltshilfe', syllables: ['Haus', 'halts', 'hil', 'fe'] },
    { full: 'zufrieden', syllables: ['zu', 'frie', 'den'] },
    { full: 'angerufen', syllables: ['an', 'ge', 'ru', 'fen'] },
    { full: 'Gewerkschaft', syllables: ['Ge', 'werk', 'schaft'] },
  ],
  // the title / header of the exercise
  title: 'Silben-Puzzle',
  // a sentence or short paragraph to describe the exercise
  description: 'Schiebe die Silben in die leeren Kästchen darunter.',
  // link to the next exercise / page
  next: '#this-link-does-not-lead-anywhere-yet',
  // labels that are shown as links to the next word / exercise (or page)
  nextWordLabel: 'Nächstes Wort',
  nextLabel: 'Nächste Übung',
  // directory where audio files for words and syllables are stored
  audioSource: '../audio/',
}
