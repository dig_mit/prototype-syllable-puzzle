# Prototype for syllable learning exercise

This is the prototype for a syllable puzzle exercise for the
[dig_mit!](https://gitlab.com/dig_mit) project.

## Developer setup

For development we use ESLint for code linting. To set this up, you need
[Node.js](https://nodejs.org) installed (ideally in version 16 or higher).

Then you can install the dev dependencies with:

```bash
npm install
```

Make sure that your IDE supports ESLint. For VS Code there is e.g. the official
[ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
extension. Once it is installed, your code should be properly linted.

If you want to use the linter manually you can do so with `npx eslint filename.js`.
Check out the [Command Line Interface](https://eslint.org/docs/user-guide/command-line-interface)
docs of ESLint.

## WordPress plugin integration

To integrate the exercise into the [dig\_mit exercises](https://gitlab.com/dig_mit/dig_mit-exercises-plugin)
WordPress plugin, put the exercise class from `index.js` as well as the
document-ready function below it into the plugin folder as
`src/js/ex-syllable-puzzle.js` and add
the following two lines above the class:

```javascript
let $ = jQuery
let $container = $( '#digmit-exercise-container-'+digMitExerciseID  )
```

These are needed, to identify the container based on the exercise id, which
is inserted by the plugins backend part, and because by default WordPress
does not provide the common jQuery shorthand `$`.

## License

The prototype code is licensed under an AGPLv3 license.

It uses [jQuery](https://jquery.com/) and [jQuery UI](https://jqueryui.com/).
Both are licensed under a MIT license.

To make the draggables mobile ready, we also use the
[jQuery UI Touch Punch](https://github.com/RWAP/jquery-ui-touch-punch)
script, originally developed by [furf](https://github.com/furf)
and kept up to date now by [RWAP](https://github.com/RWAP).
_jQuery UI Touch Punch_ is dual licensed under a MIT and GPLv2 license.
